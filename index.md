---
layout: book
---
{% assign ceremonies = site.collections | where: "label", "ceremonies" | first %}

<div class="single">


<h1>
<br><br><br>
{{ site.title }}
<br><br>
<img src="{{ site.baseurl }}/img/logo.svg" width="60%" class="center">
<br><br><br><br><br>
</h1>
<h4>
{{ site.time | date_to_long_string }}<br>
</h4>

</div>

<div class="single">
<p>
Source {{ site.url }}
</p>

<p>
Current as of {{ site.time | date_to_long_string }}
</p>

<p>
Build {{ site.buildnumber | slice: 0, 8 }}
</p>

</div>

<div class="single">

<h1>Table of Contents</h1>

<ol>
{% for ceremonytype in ceremonies.ceremonytypes %}
  {% for ceremonyhash in ceremonytype %}
    {% assign chapterslug = ceremonyhash[0] %}
    {% assign chaptertitle = ceremonyhash[1] %}
  {% endfor %}
    <h3><li>{{ chaptertitle }}</li></h3>
    <ol>
  {% assign ceremonydocs = ceremonies.docs | where: "type", chapterslug %}
  {% for ceremony in ceremonydocs %}
    <li><a href="#{{ ceremony.slug }}">{{ ceremony.title }}</a></li>
  {% endfor %}
    </ol>
{% endfor %}
</ol>

</div>

{% for ceremonytype in ceremonies.ceremonytypes %}

  {% for ceremonyhash in ceremonytype %}
    {% assign chapterslug = ceremonyhash[0] %}
    {% assign chaptertitle = ceremonyhash[1] %}
  {% endfor %}

<div class="single">
<h1>
<br><br><br><br><br><br><br>
Chapter {{ forloop.index }}<br><br>
{{ chaptertitle }}
</h1>
</div>

  {% assign ceremonydocs = ceremonies.docs | where: "type", chapterslug %}
  {% for ceremony in ceremonydocs %}

<div class="single">
<a name="{{ ceremony.slug }}"></a>
<h1>{{ ceremony.title }}</h1>

{{ ceremony.output }}
</div>

  {% endfor %}
{% endfor %}
