---
title: "Oath of Fealty to Drachenwald"
type: succession
---
_Intended for use at Drachenwald Coronation, otherwise the Oath in the Rite of Succession can be used_

**Herald**  
We {% include fillin.html %} and {% include fillin.html %} Prince and Princess of Insulae Draconis, rightful and lawful holders of the fief of Insulae Draconis, do acknowledge you, {% include fillin.html %} and{% include fillin.html %} as the heirs and successors of {% include fillin.html %} and {% include fillin.html %}, to whom I/We swore fealty as vassal for the aforesaid fief of Insulae Draconis, and We will hold you as Our rightful King and Queen, binding Our heirs and successors in like wise to be Your vassals for the aforesaid fief, delivering to you all those things which are Yours by right of law and custom and reserving those ancient liberties and customs as established in the time of Your progenitors.

**Prince**  
So swear we {% include fillin.html %}, Prince.

**Princess**  
So swear we {% include fillin.html %}, Princess.