---
title: "Scroll text for the Order of the Fox"
type: scrolltext
---
Know ye, that whereas the defence of Our Lands and the keeping of Our Principality’s peace are foremost in Our concerns, being mindful of Our coronation oaths, we
{% include fillin.html %} and {% include fillin.html %}, Prince and Princess of Insulae Draconis, therefore to the furtherance of the said defence and peace of Insulae Draconis, hereby increase our Order of the Fox by addition of {% include fillin.html %} to their number, charging the Said {% include fillin.html %} to maintain arms as set down in our assizes and to set himself under any Justice of the peace of his shire in such matters as are lawful just and necessary.

By our Hands this {% include fillin.html %}th day of {% include fillin.html %}, A.S. {% include fillin.html %} at (event) in (shire name)

{% include fillin.html %} Prince {% include fillin.html %} Princess
