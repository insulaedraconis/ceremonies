---
title: "Scroll text for the Order of the Sun and Chalice"
type: scrolltext
---
{% include fillin.html %} and {% include fillin.html %}, By Grace of God Prince and Princess of Insulae Draconis, unto all these present lettres seeing reading or hearing know, whereas our Forebears in time past have founded the most ancient and honourable Order of the Sun and Chalice to be comprised of those who most enrich our realm by their deeds, achievements, bearing and franchise therefore do we on account of his/her own bearing, franchise and many deeds and Accomplishments witnessed and attested to by very many Peers, Barons, Lords and Commons alike of these lands of Insulae Draconis in keeping with the Law and customs of the land enrol into the most Honourable Company of Excellent Example {% include fillin.html %} with all the customary attendant benefices rights and honours, including but not limited to the right to bear the token of Said company, to whit the Sun and Chalice from this day forth.

Done by our hands, this {% include fillin.html %}th day of {% include fillin.html %}, A.S. {% include fillin.html %} at _(event)_ in _(shire name)_

{% include fillin.html %} Prince {% include fillin.html %} Princess