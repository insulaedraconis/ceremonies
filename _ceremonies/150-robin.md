---
title: "Order of Robin"
type: awards
---
**Herald**  
Will all members of the Order of Robin stand.

Those who are wise and know of the adages taught in the bestiaries know that in those books the robin is not mentioned. Why might this be? Why because it is of such good courtesy that words cannot describe him, nor poetry ensnare him in description.

A redbreast brings sustenance to the hen it courts; it cares for its young; in all manner of things it bears itself with gentility. It is without like in virtue.

In these lands of Insulae Draconis there was once such a man who embodied these virtues in such measure that it was said that Robin, called Bowman, outshone even the bird his namesake. In memory of this most gentle of men was thus created the Order of Robin, to reward those whose actions are likewise incomparable in their courtesy.

And so do Their Highnesses call before them {% include fillin.html %}.

_[TCH may make personal statements, the Herald reads scroll, or scroll text]_

For the newest member of the Order of Robin, Vivat! Vivat! Vivat!