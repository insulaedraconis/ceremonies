---
title: "Retinue Fealty Oaths"
type: succession
---
**Herald**  
Let those members of Their Highnesses retinue who would swear fealty to them now forward and make reverence before them.

_[Members of retinue come forward.]_

**Herald**  
Will you hold {% include fillin.html %} and {% include fillin.html %} your rightful Prince and Princess until the end of Their lawful reigns?

**Retinue**  
I so swear.

**Herald**  
Will you honour Them, assist them in their endeavours, and support Their Royal might and justice, obeying Their commands in such matters as are Theirs to command?

**Retinue**  
I so swear.

**Prince**  
We do hear and accept the fealty of these, Our retinue. We in turn affirm to protect and defend you with Our royal might and justice. So say we {% include fillin.html %}, Princeps.

**Princess**  
And so say We {% include fillin.html %}, Princepissa.