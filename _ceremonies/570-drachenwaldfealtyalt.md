---
title: "Alternative Oath of Fealty to Drachenwald"
type: succession
---
_Adapted from the Kingdom book of Ceremonies_

**Herald**  
Let all present and to come know that We {% include fillin.html %}, Prince of Insulae Draconis and {% include fillin.html %}, Princess of Insulae Draconis, acknowledge verily to You, {% include fillin.html %} and {% include fillin.html %}, King and Queen of Drachenwald, and to your successors that We hold and ought to hold as a fief the Principality of Insulae Draconis.

For this We make homage and fealty with hands and with mouth to You, Our King and Queen, and to Your successors, and We swear that We will always be faithful vassals to You and to Your successors in all things in which a vassal is required to be faithful, and We will defend You and all Your successors, the Kingdom and Insulae Draconis against all malefactors and invaders, and We will give You power over all the castles and manors in the Principality, in peace and in war, whenever they shall be claimed by You or by Your successors.

And if We or Our heirs or their successors do not observe to You or to Your successors each and all the things declared before, and should come against these things, We wish that the aforesaid fief should by that very fact be handed over to You and to the Kingdom of Drachenwald and to Your Successors.

**Prince**  
So say we {% include fillin.html %}, Prince.

**Princess**  
And so say we {% include fillin.html %}, Princess.
