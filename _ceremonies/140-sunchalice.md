---
title: "Order of the Sun and Chalice"
type: awards
---
**Herald**  
Will all members of the Order of the Sun and Chalice come forward.

_[Members of the order make their way into the Presence.]_

As the Prince and Princess serve the people of Insulae Draconis throughout their tenure, they set an example of glory for all the people of the land. However they are not alone in being mighty examples of the excellence to which we all aspire.

There are those among us, who, by their very presence, enhance our Current Middle Ages to the betterment of all, by their demeanour, manners and enthusiasm for the activities we participate in.

Thus was formed the Order of the Sun and Chalice to hold up such individuals for all to see and strive to match and excel. The responsibilities of this company can be grave as they are expected to advise the Prince and Princess on many matters for the gain and growth of our Principality.

And so do the Their Highnesses call before them {% include fillin.html %}.

_[Coronet may make personal statements, the Herald reads the scroll or scroll text]_

For the newest member of the Order of the Sun and Chalice, Vivat! Vivat! Vivat!

{% include fillin.html %}, pray now greet the other members of the Order.

_[After the group hug]_

The Order has Their Highnesses’ leave to depart.