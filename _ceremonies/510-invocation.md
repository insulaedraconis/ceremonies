---
title: "Invocation of Principality Coronet Lists"
type: succession
---
**Herald**  
Their Highnesses command that all entrants to the tournament come forward and present themselves and their consorts before the Coronets of Insulae Draconis.

_In order of precedence, each fighter proceeds one by one in front of the Coronet, their chosen herald (if present), announces them to both the Coronet and the populace, they kneel and the entrant introduces their consort to the Prince & Princess.)_

**Herald**  
My Lords and Ladies, you know well that true gentles enter combat not for their own personal glory, but rather to advance the honour of their consorts; and to prove by their courtesy and valour upon the field, their being worthy of said honour.

Keep in your heart as you fight upon the field today your consort’s honour as well as your own; for your consorts personify all honour, glory, strength, beauty and truth. They embody the one single great thing that is worthy of reward and that is love.

Therefore, today as you fight to become the Prince or Princess of Insulae Draconis remember that you do not fight to gain power for yourself, nor for just your own glory and honour. Instead, you fight for the honour and glory of the one who has granted you a favour as a token of love and esteem and whom you would advance by your valour, skill and courtesy.

Thus should victory be granted unto you this day, you and your consort will be declared as the next Prince and Princess of the fair lands of Insulae Draconis.

**Prince**  
You have heard the reasons for Our Lists. Each of you here today have a lady or lord whose favour you would advance, whom you would make Princess or Prince as your consort, should you prove yourself victorious upon the field. Is it still your desire to participate in these lists?

**Entrants**  
It is Your Highness

**Princess**  
Do each of you now affirm that you and your consort, fulfil all the requirements for fighting this day as set forth by Our Laws?

**Entrants**  
We do Your Highness

**Prince**  
And do you all here know the Rules of the Lists and swear to abide by them?

**Entrants**  
We do Your Highness

**Princess**  
Then bear your favours bravely into the field and prove the worth and virtue of your consorts, may it be in victory or in honourable defeat. You have Our Leave to withdraw and prepare for the tournament.