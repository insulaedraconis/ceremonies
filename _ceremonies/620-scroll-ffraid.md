---
title: "Scroll text for the Order of Ffraid"
type: scrolltext
---
Praise and just reward are due to those who, like unto Blessed St Ffraid who in past time by miraculous increase fed the needy, do labour to the common profit of the realm, by charitable acts of corporal mercy and civic labours both. Therefore was our Order of Ffraid established that virtuous men/women, especially discreet and capable of labouring, might by their merits and renown be known to all. We {% include fillin.html %} and {% include fillin.html %} Prince and Princess of Insulae Draconis, finding our Noble subject {% include fillin.html %} to be virtuous and deserving of such honours do invest them in our order of Ffraid to join the number of that noble company in observing its Ordinances.

Given at {% include fillin.html %} in {% include fillin.html %} shire, the {% include fillin.html %} day of our reign.