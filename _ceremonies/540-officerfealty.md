---
title: "Oaths of Fealty of Principality Officers"
type: succession
---
**Herald**  
Let those Officers of the Principality here present who would swear their fealty to Their Highnesses come forward.

_[Officers come forward.]_

You shall swear to bear true and faithful councillor to their Highnesses as one of their Highnesses’ Officers.

You shall not know or understand of any thing to be attempted, done or spoken about Their Highnesses’ person, honour, crown or dignity royal, but you shall let and withstand the same to the uttermost of your power, and either do or cause it forthwith to be revealed either to Their Highnesses’ self or to the rest of Their Privy Council.

You shall keep secret all matters committed and revealed to you as Their Highnesses’ councilor or that shall be treated of secretly in council.

And if any of the same treaties or counsels shall touch any other of the officers, you shall not reveal the same to him, but shall keep the same until such time as by the consent of their Highnesses, or of the rest of the council, publication shall be made thereof.

You shall not let to give true, plain and faithful counsel at all times, without respect either of the cause or of the person, laying apart all favour, meed, affection and partiality.

And you shall to your uttermost bear faith and true allegiance to their Highnesses, their heirs and lawful successors, and shall assist and defend all jurisdictions, preeminences and authorities granted to their Highnesses and annexed to the crown, against all foreign princes, persons, prelates or potentates, whether by act of parliament or otherwise.

And generally in all things you shall do as a faithful and true councillor ought to do to their Majesties. So help you to all good and by the form and contents of this book.

**Officers**  
We so swear.

**Prince**  
And We swear in these things to aid and support you in your Office(s) after the course and form of our Laws and Customs used in like case afore this time.

So say we, {% include fillin.html %} Prince.

**Princess**  
And so say we, {% include fillin.html %} Princess.