---
title: "Viscounty"
type: awards
---
**Herald**  
Let {% include fillin.html %} and {% include fillin.html %} present themselves before their Highnesses, for they have greatly enriched Our Principality, and We would honour them.

_Former Prince and Princess come forward._

**Herald**  
It is written that, heavy is the burden of the crown, and so it is, not from fear of treachery in this peaceful land, but from the weight of responsibility. As such, it is meet that those who have passed to other hands the rights and duties of the Coronet be rewarded for their attainments and service.

**Prince**  
{% include fillin.html %}, having won the Crown of this Our beloved principality, and having ruled honourably as Prince, We do hereby affirm you as Viscount. Will you accept this honour from Our Hand?

**Viscount**  
I will, Your Highness.

**Princess**  
{% include fillin.html %}, having by your beauty, grace, and virtue inspired your champion to win for you the Coronet of this, Our beloved Principality and having ruled as its gracious Princess, We do hereby affirm you as Visountess. Will you accept this honour from Our Hand?

**Viscountess**  
I will, Your Highness.

**Prince**  
In consensus with the Peerage, make known Our Royal will.

_[If the scrolls are available, the herald reads each of them aloud. If not, read the scroll text below]_

**Herald**  
Know all men by these presents that We, {% include fillin.html %} and {% include fillin.html %}, Prince and Princess of Insulae Draconis, in recognition that {% include fillin.html %} and {% include fillin.html %}, have reigned as Prince and Princess of these Dragon Isles, do hereby style them Viscount and Viscountess. By Our hands and seal this {% include fillin.html %} day of {% include fillin.html %}, Anno Societatus ------.

{% include fillin.html %}, Princeps; {% include fillin.html %}, Principissa

**Herald**  
For {% include fillin.html %} and {% include fillin.html %}, newest Viscount and Viscountess of these Dragon Isles, Vivant! Vivant! Vivant!