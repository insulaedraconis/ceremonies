---
title: "Scroll text for the Order of the Silver Martlet"
type: scrolltext
---
Forasmuch as Artisans, Artificers, Scholars, Craftsmen, Clerks, Cooks, (Include the art of the recipient here) and all others of fruitful and creative profession are greatly necessary to the prosperity of our realm, and that the increase of craft mysteries and secrets amongst the aforesaid Artisans et Alia being likewise necessary to their continuance, We {% include fillin.html %} and {% include fillin.html %} Prince and Princess of Insulae Draconis, do require our Worshipful Company of Artisans to admit {% include fillin.html %} as a member of the Order of the Silver martlet that they may lawfully share the mysteries and secrets of (Recipients art) under the laws, articles and customs of the Order.

By our Hands this {% include fillin.html %}th day of {% include fillin.html %}, A.S. {% include fillin.html %} at (event) in (shire name)

{% include fillin.html %} Prince {% include fillin.html %} Princess
