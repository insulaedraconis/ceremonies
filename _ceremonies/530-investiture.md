---
title: "Investiture"
type: succession
---
_[If space and venue allow, seating can be arranged for Barons and Baronessess to be seated on the right of the Thrones, Peers to the left. Their Majesties will enter first, announced by their own herald.]_

**Herald**  
All rise All rise, and give reverence to Their Highnesses {% include fillin.html %} and {% include fillin.html %},Prince and Princess of our fair lands of Insulae Draconis, Marquis and Marchioness of the Lands of Endor’s Keep in the Far Isles, sworn vassals of Their Majesties Drachenwald, {% include fillin.html %} and {% include fillin.html %}.

_[Entry of TCH]_

You have Their Highnesses leave to be seated. Here opens the last court of Their Celestial Highnesses {% include fillin.html %} and {% include fillin.html %} Princepissaque Insularum Draconis.

_[Normal court business, including dismissal of Retainers if necessary]_

**Prince**  
We stand before you as your rightful Prince and Princess, Invested as such by our predecessors {% include fillin.html %} and {% include fillin.html %} and acknowledged by King {% include fillin.html %} and Queen {% include fillin.html %} in like wise.

**Princess**  
In keeping with law, custom and our investiture oaths, we are bound to cede our coronets and this Our fief of Insulae Draconis to our lawful successors. Therefore Rejoice, for We have Heirs Apparent, and their hour is come.

_[Optional Processional (Gaudete) for entrance of the Heirs. Enter Heirs.]_

**Herald**  
Come now before you the Heirs Apparent to the Coronet of Insulae Draconis; {% include fillin.html %} and {% include fillin.html %}.

_[Heirs kneel some way before Coronet]_

**Herald**  
{% include fillin.html %} and {% include fillin.html %}, you were triumphant at Coronet Tourney and are thus favoured to rise to the dignity of Prince and Princess, in earnest hope that by your rule these lands shall remain free and by the gift of peace be protected. On that day you took to yourselves the title of Lord and Lady of these lands, heirs apparent to the Coronets of Insulae Draconis. Do you still claim that Right?

**Heirs**  
We do.

_[Heirs Place hands between the hands of outgoing Prince and Princess]_

**Principality Seneschal**  
{% include fillin.html %} and {% include fillin.html %}, will you hold Yourselves to the laws and customs passed on to you by your predecessors and by the Barons and people of Insulae Draconis and observe their works?

**Heirs**  
I will.

**Principality Seneschal**  
Will you be the protector and defender of the Principality’s Officers and their Ministries?

**Heirs**  
I will.

**Principality Seneschal**  
Will you rule and defend through justice the Principality of Insulae Draconis, held from the Crown of Drachenwald as a fief?

**Heirs**  
I promise that I will in all respects faithfully observe this oath.

**Herald**  
Oh ye people of Insulae Draconis, shall you subject yourselves to such a Prince and Princess as rulers to continue this principality, to anchor by firm faith and also to obey those commandments you might owe? Then cry let it be done! Let it be done!

**Court Response**  
Let it be done! Let it be done!

**Herald**  
Now shall Your hands be anointed with this balm, of the earth of your field of victory, and of all other fields where the Coronets of these lands have been contested. For as you shall hold these lands in your hands, let there be bodily health in the land.

**Principality Seneschal**  
I anoint you with this balm before your people, in the sight of your predecessors.

_[Anoint both heirs on palms. The balm is the earth of the tourney field mixed with oil.]_

**Prince**  
As you are the heirs apparent, acclaimed by the people, blessed by tradition and acknowledged by law and the officers of state, we can hold these Coronets no Longer.

**Herald**  
These Coronets signify the Royal glory and honour of these Dragon Isles, the work of courage and the value of inspiration. They proclaim you in all things the true supporters of law and the vigorous defenders of society against all adversities. You should appear the suitable executors and profitable rulers of the principality given to you as a fief, that raised up among the glorious, adorned with the jewels of virtue, and crowned with the reward of your labours, you may bring glory without end unto this Principality of Insulae Draconis.

**Princess**  
We accept and acknowledge you as our rightful successors.

**Prince**  
Accept the coronets of the principality, placed by our hands upon your heads.

_[Outgoing Prince and Princess place Coronets upon incoming Prince and Princess.]_

**Herald**  
Do you, {% include fillin.html %} and {% include fillin.html %},Prince and Princess of Insulae Draconis, rightful and lawful holders of the fief of Insulae Draconis, hereby acknowledge {% include fillin.html %} and {% include fillin.html %} the heirs and successors of {% include fillin.html %} and {% include fillin.html %} as King and Queen of Drachenwald. Do you acclaim Them as your rightful liege lords to whose line of succession our ancient predecessors William and Æringunnr, the heirs and successors of Thorvaldr and Fiona as Prince and Princess of Insuae Draconis, swore fealty as vassals for the said fief of Insulae Draconis?

**Prince and Princess**  
So swear we.

**Herald**  
Will you hold {% include fillin.html %} and {% include fillin.html %} and Their heirs and successors, to be your rightful King and Queen, binding you, and your heirs and successors, in like wise to be vassals for the aforesaid fief of Insulae Draconis, delivering to Them all those things which are Theirs by right of law and custom and reserving those ancient liberties and customs as established in the time of your progenitors?

**Prince and Princess**  
So swear we.

**Herald**  
Oh ye people of Insulae Draconis, is this heard and witnessed?

**Omnes**  
Heard and Witnessed! Aye! It is! etc

**Former Princess**  
Rise up, and reign in your own right.

**Former Prince and Princess**  
We greet you, our Prince and Princess.

_[Kiss of Peace. Prince and Princess take thrones.]_

**Prince and Princess**  
{% include fillin.html %} and {% include fillin.html %} you have our thanks and those of the Principality. Go now, with our blessing.

_[Former Prince and Princess retire]_

**Herald**  
For {% include fillin.html %} {% include fillin.html %}, who ruled with strength and grace but who have now put aside the honour and burden of the Coronets, Vivat! Vivat! Vivat!