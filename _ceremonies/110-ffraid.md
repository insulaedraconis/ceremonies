---
title: "Order of Ffraid"
type: awards
---
**Herald**  
Will all members of the Order of Ffraid please stand.

As the Prince and Princess serve the people of Insulae Draconis throughout their tenure, they set an example of service for all the people of these lands. All who enjoy the beauty and prosperity that is Insulae Draconis are expected to render service when required, to the best of their abilities.

There are those few, however, whose service consistently exceeds the expectations of their offices or ranks and who are, therefore, especially worthy of reward.

The Order of Ffraid was created to honour the memories of those whose devotion, determination and selflessness is presented as exceptional in many a tale. By this example, it is given to those persons who have served Insulae Draconis above and beyond the normal contributions of any other gentle.

And so do Their Highnesses call before them {% include fillin.html %}.

_[TCH may make personal statements, the Herald reads scroll or scroll text]_

For the newest member of the Order of Ffraid, Vivat! Vivat! Vivat!