---
title: "Introduction"
type: introduction
---
This is the Insulae Draconis Book of Ceremonies.

It is based on the third edition, which was prepared by Ysabella-Maria Vasquez de Granada y Cortes and published in August 2013. 

The first edition, prepared by Lord Jean-Loup de Fribois and Lord Robin Bowman of Dalriada, was written for Insulae Draconis as a Crown Principality and, following Insulae Draconis becoming a Principality, a revision was needed. The second edition was orchestrated by Lord Asbiorn inn eyverski, Rockall Herald, with a great deal of assistance in creation of the new ceremonies provided by Master Robert of Canterbury. Further assistance in various stages of the project was provided by other members of Drachenwald's College of Heralds within Insulae Draconis.

The ceremonies, as written here are intended as a base for Court Heralds to work from. They can be used exactly as written, but particularly for the minor ceremonies, alterations as needed are to be encouraged to prevent courts becoming stale and repetitive. There are currently alternative versions provided for the Principality Officer's Oath and the Coronet's Oath of Fealty to the Crown, plus alternative versions of the Investiture of the Heirs. Which of these you use is up to you, and the Coronet you are working with.

If any members of the College of Heralds within the Principality have possible alternative versions of any of these ceremonies (from particular cultures, based upon particular period examples, etc) they should send them to Rockall (herald@insulaedraconis.org) for consideration for inclusion.

The following abbreviations are used in this book:

TRM Their Royal Majesties  
TCH Their Celestial Highnesses

Comments, explanations and options are given in italics and brackets.