---
title: "Entrance into Court and Opening of Court"
type: entranceexit
---
**Herald [In procession]**  
All rise All rise, and give reverence to Their Celestial Highnesses {% include fillin.html %} and {% include fillin.html %}, Prince and Princess of our fair lands of Insulae Draconis, Marquis and Marchioness of the Lands of Endor’s Keep in the Far Isles, sworn vassals of Their Majesties Drachenwald, {% include fillin.html %} and {% include fillin.html %}

_[This can be used either for a Court solely for TCH, or in the entrance to a Royal Court once the Royal herald has announced TRM.]_

**Herald [Once TCH are seated]**  
Here commences the Court of Their Celestial Highnesses {% include fillin.html %} and {% include fillin.html %}, by Right of Arms Prince and Princess of Insulae Draconis.

Your Highnesses, does the court have leave to take their ease?

_[Assuming TCH agree]_

Their Highnesses bid that you take your ease.