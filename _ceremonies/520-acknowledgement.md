---
title: "Acknowledgement of the Heirs"
type: succession
---
**Herald**  
Let the participants in today’s tournament and their consorts present themselves before their Highnesses.

_An opportunity for the Coronet to address the Entrants_

**Princess**  
Let {% include fillin.html %} and {% include fillin.html %} come before us.

Barons, Peers and assembled nobility of Insulae Draconis, rejoice. Rejoice and give thanks for we are delivered of Heirs to the Principality of Insulae Draconis.

**Herald**  
We {% include fillin.html %} and {% include fillin.html %}, Prince and Princess of Insulae Draconis hereby declare {% include fillin.html %} and {% include fillin.html %} our heirs apparent to our Fief of Insulae Draconis, to be invested this day in our Coronet Court.

**Prince**  
Bear these Chains of estate, as you are bound to the Succession.

_Victors recieve Viceregal Chains_

**Princess**  
Rise now, and go forth, to await our summons to your investiture.

**Herald**
For the Heirs Apparent; _Vivant/Gaudete etc_