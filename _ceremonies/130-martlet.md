---
title: "Order of the Silver Martlet"
type: awards
---
**Herald**  
Will all members of the Order of the Silver Martlet stand.

Our Principality has always prided itself on the abundance of skilled gentles that inhabit these lands. However, when an individual has been seen to excel in such skills and scholarly expertise, when they have not only displayed their own knowledge but also shared it with others, it becomes the solemn yet joyful duty of the Coronet to reward them and thereby encourage others to further develop their own skills, whatever they may be.

History is made of such gentles whose artistic talent were well known all over the world. Yet history would not be what it is without those who first set to write it.

Therefore was created the Order of the Silver Martlet, to be granted to individuals bring to increase the beauty and wisdom of Insulae Draconis.

And so do Their Highnesses call before them {% include fillin.html %}.

_[TCH may make personal statements, the Herald reads scroll, or scroll text]_

For the newest member of the Order of the Silver Martlet, Vivat! Vivat! Vivat!