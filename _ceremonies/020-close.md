---
title: "Closing of Court and Exeunt"
type: entranceexit
---
**Herald**  
Your Highnesses, have you any further business?

_[Assuming not]_

There being no further business of the Court, the Court is closed.

All rise!

Long live Their Majesties!

_[If there are currently Royal Heirs]_ Long live Their Royal Highnesses!

Long live Their Highnesses!

_[If there are currently Heirs to the Principality]_ Long live The Heirs!

Long live Drachenwald!

Long live Insulae Draconis!