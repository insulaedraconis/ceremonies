FROM jekyll/jekyll:3
VOLUME ["/build"]
WORKDIR /build
EXPOSE 4000
CMD bundle install && bundle exec jekyll serve --watch --host 0.0.0.0 --config _config.yml,_build/_config-build.yml